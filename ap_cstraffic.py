#encoding: utf-8
from __future__ import division
import numpy as np
import csv
import operator


ap_list = {}
def accumulate_total_ap_traffic(total_traffic):

	global ap_list
	
	for ap in total_traffic:
		print(ap[0])

		val = ap_list.get(ap[0], (0,0))


		ap_list[ap[0]] = (int(val[0]) + int(ap[1]), 0)

def accumulate_total_cs_traffic(cs_traffic):

	global ap_list
	
	for ap in cs_traffic:
		
		val = ap_list.get(ap[0])
		ap_list[ap[0]] = (val[0], int(val[1])+ int(ap[1]))

with open('aplist/aplisttotal1400.csv') as f:
	 
	 total_traffic = csv.reader(f, delimiter=',')
	 accumulate_total_ap_traffic(total_traffic)

with open('aplist/aplisttotal1530.csv') as f:
	 
	 total_traffic = csv.reader(f, delimiter=',')
	 accumulate_total_ap_traffic(total_traffic)


with open('aplist/aplisttotal0930.csv') as f:
	 
	 total_traffic = csv.reader(f, delimiter=',')
	 accumulate_total_ap_traffic(total_traffic)


with open('aplist/aplist1400.csv') as f:

	cs_traffic = csv.reader(f, delimiter=',')
	accumulate_total_cs_traffic(cs_traffic)


with open('aplist/aplist0930.csv') as f:

	cs_traffic = csv.reader(f, delimiter=',')
	accumulate_total_cs_traffic(cs_traffic)


with open('aplist/aplist1530.csv') as f:

	cs_traffic = csv.reader(f, delimiter=',')
	accumulate_total_cs_traffic(cs_traffic)


ap_percentage_list = {}

for key, value in ap_list.iteritems():
	ap_percentage_list[key] = value[1]/value[0]
	



sorted_ap_list = sorted(ap_percentage_list.items(), key=operator.itemgetter(1), reverse=True)

for value in sorted_ap_list:
	print value
		




