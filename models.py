from peewee import *

# db = SqliteDatabase('cpsc527.db', threadlocals=True)

db = PostgresqlDatabase(
    'cpsc527', 
    user='cpsc527',  
    password='networkawesomeness',  
    host='localhost',)

class Packet(Model):
    mac_address = CharField()
    timestamp = DateTimeField()
    src_ip = CharField()
    dst_ip = CharField()
    protocol = CharField()
    src_port = IntegerField(null=True)
    dst_port = IntegerField(null=True)
    from_cs = BooleanField()
    to_cs = BooleanField()
    application_type = CharField(default='OTHER')
    size = IntegerField()

    class Meta:
        database = db # This model uses the "people.db" database.


db.connect()

#try:
#    Packet.create_table()
#except OperationalError:
#   print "Packet table already exists!"