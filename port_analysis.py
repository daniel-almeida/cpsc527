#encoding: utf-8
from __future__ import division
import numpy as np
import csv
import operator


port_list = {}


def accumulate_total_cs_traffic(cs_traffic):

	global port_list
	
	for port in cs_traffic:
		
		val = port_list.get(port[0], 0)
		port_list[port[0]] = int(val) + int(port[1])

with open('port/test1400.csv') as f:

	cs_traffic = csv.reader(f, delimiter=',')
	accumulate_total_cs_traffic(cs_traffic)


with open('port/test0930.csv') as f:

	cs_traffic = csv.reader(f, delimiter=',')
	accumulate_total_cs_traffic(cs_traffic)


with open('port/test1530.csv') as f:

	cs_traffic = csv.reader(f, delimiter=',')
	accumulate_total_cs_traffic(cs_traffic)



for key, value in port_list.iteritems():
	print key, value
	



sorted_port_list = sorted(port_list.items(), key=operator.itemgetter(1), reverse=True)

for value in sorted_port_list:
	print value
		




