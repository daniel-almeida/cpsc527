import socket
import re

#returns hostname of ip. more accurate?
def get_hostname(ip):
  try:
    name, bigip, x = socket.gethostbyaddr(ip)
    return name
  except socket.herror:
    return None

#returns application type for non-null hostname
def check_hostname(hostname):
  if re.search(r'facebook', hostname) or re.search(r'fbcdn', hostname):
    return 'Facebook'
  
  if re.search(r'reddit', hostname): 
    return 'Reddit'

  if re.search(r'whatsapp', hostname): 
    return 'Whatsapp'

  if re.search(r'imgur', hostname): 
    return 'Imgur'

  return 'Other'

#returns application for ip address given pre-set IP address lists
def check_ip(ip):
  if ip in fb_ip_list:
    return 'Facebook'
  if ip in reddit_ip_list:
    return 'Reddit'
  if ip in quora_ip_list:
    return 'Quora' 
  return 'Other'

#gets application type for given ip address
def get_application(ip):
  hostname = get_hostname(ip)
  if hostname == None:
    return check_ip(ip)
  return check_hostname(hostname)

fb_ip_list= []
reddit_ip_list= []
quora_ip_list= []

quora_ip_list.append('54.84.216.68')
quora_ip_list.append('54.85.52.131')
reddit_ip_list.append('198.41.209.142')
reddit_ip_list.append('198.41.209.137')
reddit_ip_list.append('198.41.208.141')
reddit_ip_list.append('198.41.208.140')
reddit_ip_list.append('198.41.209.138')
reddit_ip_list.append('198.41.208.137')
reddit_ip_list.append('198.41.209.140')
reddit_ip_list.append('198.41.208.142')
reddit_ip_list.append('198.41.209.141')
reddit_ip_list.append('198.41.209.143')
reddit_ip_list.append('198.41.208.138')
reddit_ip_list.append('198.41.209.139')
reddit_ip_list.append('198.41.209.143')
reddit_ip_list.append('198.41.209.136')
reddit_ip_list.append('198.41.209.125')

 #Facebook AS :

# Add   31.13.24.0/21   (31.13.24.00 - 31.13.31.255)
for bignum in range(24, 31):
    for smallnum in range(0,255): 
      fb_ip_list.append('31.13.' + str(bignum) + '.' + str(smallnum))

# Add   31.13.64.0/18  (31.13.64.00 - 31.13.95.255)
for bignum in range(64, 64+63):
    for smallnum in range(0,255): 
      fb_ip_list.append('31.13.' + str(bignum) + '.' + str(smallnum))

# Add  31.13.96.0/19 (31.13.96.00 - 31.13.95.255)
for bignum in range(96, 96+31):
    for smallnum in range(0,255): 
      fb_ip_list.append('31.13.' + str(bignum) + '.' + str(smallnum))

# Add  204.15.20.0/22 (204.15.20.0 - 204.15.23.255)
for bignum in range(20, 23):
    for smallnum in range(0,255): 
      fb_ip_list.append('204.15.' + str(bignum) + '.' + str(smallnum))

# Add  69.63.176.0/20 (69.63.176.0 - 69.63.191.255)
for bignum in range(176, 191):
    for smallnum in range(0,255): 
      fb_ip_list.append('69.63.' + str(bignum) + '.' + str(smallnum))

# Add  66.220.144.0/20  (66.220.144.0 - 66.220.159.255)
for bignum in range(144, 159):
    for smallnum in range(0,255): 
      fb_ip_list.append('69.220.' + str(bignum) + '.' + str(smallnum))

# Add  74.119.76.0/22   (74.119.76.0 - 74.119.79.255)
for bignum in range(76, 79):
    for smallnum in range(0,255): 
      fb_ip_list.append('74.119.' + str(bignum) + '.' + str(smallnum))

# Add   69.171.224.0/19   ( 69.171.224.0 - 69.171.255.255)
for bignum in range(224, 255):
    for smallnum in range(0,255): 
      fb_ip_list.append('69.171.' + str(bignum) + '.' + str(smallnum))

# Add  129.134.0.0/16   (129.134.0.0 -  129.134.255.255)
for bignum in range(0, 255):
    for smallnum in range(0,255): 
      fb_ip_list.append('129.134.' + str(bignum) + '.' + str(smallnum))

# Add   157.240.0.0/16    ( 157.240.0.0 -  157.240.255.255)
for bignum in range(0, 255):
    for smallnum in range(0,255): 
      fb_ip_list.append('157.240.' + str(bignum) + '.' + str(smallnum))

# Add   185.60.216.0/22    ( 185.60.216.0 - 185.60.219.255)
for bignum in range(216, 219):
    for smallnum in range(0,255): 
      fb_ip_list.append('185.60.' + str(bignum) + '.' + str(smallnum))

# Add    179.60.192.0/22     (  179.60.192.0 - 179.60.195.255)
for bignum in range(192, 195):
    for smallnum in range(0,255): 
      fb_ip_list.append('179.60.' + str(bignum) + '.' + str(smallnum))

# Add     173.252.64.0/18     ( 173.252.64.0- 173.252.127.255)
for bignum in range(64, 127):
    for smallnum in range(0,255): 
      fb_ip_list.append('173.252.' + str(bignum) + '.' + str(smallnum))

# Add    103.4.96.0/22     ( 103.4.96.0  - 103.4.99.255)
for bignum in range(96, 99):
    for smallnum in range(0,255): 
      fb_ip_list.append('103.4.' + str(bignum) + '.' + str(smallnum))

# Add    45.64.40.0/22     ( 45.64.40.0  - 45.64.43.255)
for bignum in range(40, 43):
    for smallnum in range(0,255): 
      fb_ip_list.append('45.64.' + str(bignum) + '.' + str(smallnum))

