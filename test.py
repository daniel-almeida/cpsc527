import json
from peewee import *
from itertools import islice
from datetime import datetime, timedelta
from models import AP

data = {}
tcpdump_ref_line = 0
ref_timestamp = None

def read_tcpdrump(file_name, timestamp):
	global tcpdump_ref_line # stores the initial line for reading the tcpdump file

	# We suppose the client will be connected to the AP for at least 10secs
	end_time = timestamp + timedelta(seconds=10) # Adds 10 sec to get end of range

	with open(file_name) as f:
		for line in islice(f, tcpdump_ref_line, None):
			l = line.split()

			# Gets packet timestamp. Date is hardcoded because tcpdump only provides time.
			# This only works for measurements within the same day
			packet_ts = datetime.strptime(
				('2015-11-15T' + l[0][:8] + 'Z'), '%Y-%m-%dT%H:%M:%SZ'
			)

			# Getting source IP and port (if available)
			src = l[17].split('.')
			src_ip = '.'.join(src[0:4])
			src_port = None
			if len(src) > 4: # has port
				src_port = src[4].replace(":", "")
			

			# Getting destination IP and port (if available)
			dst = l[19].split('.')
			dst_ip = '.'.join(dst[0:4])
			dst_port = None
			if len(dst) > 4: # has port
				dst_port = dst[4].replace(":", "")

			length = l[-1] # TODO change this to get size inside parenthesis

			if (packet_ts >= timestamp) and (packet_ts <= end_time):
				for ap in data:
					if str(timestamp) in data[ap]:
						# ip_idx = data[ap][str(timestamp)].index()
						if src_ip in data[ap][str(timestamp)]:
							print 'creating packet'
							# data[ap][str(timestamp)][src_ip].append({
							# 		'timestamp': str(packet_ts),
							# 		'src': src_ip,
							# 		'srcPort': src_port,
							# 		'dst': dst_ip,
							# 		'dstPort': dst_port,
							# 		'length': length
							# 	})
							
				tcpdump_ref_line += 1 # Saves next line to be read
				
			elif packet_ts > end_time:
				break # breaks the loop if packet timestamp is out of the 10sec interval
					  # tcpdump_ref_line will save next line to be read for the next 10sec interval

# Gets the starting/reference timestamp
with open('sample_snmp.txt') as g:
	for line in g:
		ref_timestamp = datetime.strptime(
			line.split()[0], '%Y-%m-%dT%H:%M:%SZ'
		)
		break

# Goes over a list of AP-IP from SNMPwalk
with open('sample_snmp.txt') as f:
	for line in f:
		l = line.split()
		timestamp = datetime.strptime(
			l[0], '%Y-%m-%dT%H:%M:%SZ'
		)
		ip = l[1]
		ap = l[2]
		# try:
		# 	ap_obj = AP(mac_address=ap)
		# 	ap_obj.save()
		# except IntegrityError:
		# 	print 'AP already exists.'

		if ap not in data:
			data[ap] = {}

		if str(timestamp) not in data[ap]:
			data[ap][str(timestamp)] = []
			
		if ip not in data[ap][str(timestamp)]:
			data[ap][str(timestamp)].append(ip)

		# If timestamp read is different, go over tcpdump data for the 10sec interval that is ending
		if timestamp != ref_timestamp:
			# read_tcpdump returns False if it didn't read at least one line of a file
			# i.e., the starting line might have been out of range, which triggered the set up
			# of the next file to be read
			read_tcpdrump('sample_tcpdump.txt', ref_timestamp)
			ref_timestamp = timestamp

# print data
with open('aps_data.json', 'w') as fp:
	json.dump(data, fp)



