# tcpdump -vqnr trace_2015-11-17-14.pcap | sed ':n;N;s/\n  */ /;tn;P;D'  | python data_processing.py
# <--- SEE DB --->
# psql
# SELECT * FROM Packet;
# <-- CLEAR DB --->
# DELETE * FROM Packet;
#

from peewee import *
from itertools import islice
from datetime import datetime, timedelta
from models import Packetv2
import sys
import re

data = {}
prevdata = {}
ref_timestamp = None
discarded_packets = None;
cs_ip_list= {}
invalid = False

for num in range(2,24+1):
	cs_ip_list['142.103.' + str(num)] = True

for num in range(32,63+1):
	cs_ip_list['198.162.' + str(num)] = True


fb_ip_list= {}
reddit_ip_list= {}
quora_ip_list= {}

quora_ip_list['54.84.216.68'] = True
quora_ip_list['54.85.52.131'] = True
reddit_ip_list['198.41.209.142'] = True
reddit_ip_list['198.41.209.137'] = True
reddit_ip_list['198.41.208.141'] = True
reddit_ip_list['198.41.208.140'] = True
reddit_ip_list['198.41.209.138'] = True
reddit_ip_list['198.41.208.137'] = True
reddit_ip_list['198.41.209.140'] = True
reddit_ip_list['198.41.208.142'] = True
reddit_ip_list['198.41.209.141'] = True
reddit_ip_list['198.41.209.143'] = True
reddit_ip_list['198.41.208.138'] = True
reddit_ip_list['198.41.209.139'] = True
reddit_ip_list['198.41.209.143'] = True
reddit_ip_list['198.41.209.136'] = True
reddit_ip_list['198.41.209.125'] = True

 #Facebook AS :

# Add   31.13.24.0/21   (31.13.24.00 - 31.13.31.255)
for bignum in range(24, 31):
    for smallnum in range(0,255): 
      fb_ip_list['31.13.' + str(bignum) + '.' + str(smallnum)] = True

# Add   31.13.64.0/18  (31.13.64.00 - 31.13.95.255)
for bignum in range(64, 64+63):
    for smallnum in range(0,255): 
      fb_ip_list['31.13.' + str(bignum) + '.' + str(smallnum)] = True

# Add  31.13.96.0/19 (31.13.96.00 - 31.13.95.255)
for bignum in range(96, 96+31):
    for smallnum in range(0,255): 
      fb_ip_list['31.13.' + str(bignum) + '.' + str(smallnum)] = True

# Add  204.15.20.0/22 (204.15.20.0 - 204.15.23.255)
for bignum in range(20, 23):
    for smallnum in range(0,255): 
      fb_ip_list['204.15.' + str(bignum) + '.' + str(smallnum)] = True

# Add  69.63.176.0/20 (69.63.176.0 - 69.63.191.255)
for bignum in range(176, 191):
    for smallnum in range(0,255): 
      fb_ip_list['69.63.' + str(bignum) + '.' + str(smallnum)] = True

# Add  66.220.144.0/20  (66.220.144.0 - 66.220.159.255)
for bignum in range(144, 159):
    for smallnum in range(0,255): 
      fb_ip_list['69.220.' + str(bignum) + '.' + str(smallnum)] = True

# Add  74.119.76.0/22   (74.119.76.0 - 74.119.79.255)
for bignum in range(76, 79):
    for smallnum in range(0,255): 
      fb_ip_list['74.119.' + str(bignum) + '.' + str(smallnum)] = True

# Add   69.171.224.0/19   ( 69.171.224.0 - 69.171.255.255)
for bignum in range(224, 255):
    for smallnum in range(0,255): 
      fb_ip_list['69.171.' + str(bignum) + '.' + str(smallnum)] = True

# Add  129.134.0.0/16   (129.134.0.0 -  129.134.255.255)
for bignum in range(0, 255):
    for smallnum in range(0,255): 
      fb_ip_list['129.134.' + str(bignum) + '.' + str(smallnum)] = True

# Add   157.240.0.0/16    ( 157.240.0.0 -  157.240.255.255)
for bignum in range(0, 255):
    for smallnum in range(0,255): 
      fb_ip_list['157.240.' + str(bignum) + '.' + str(smallnum)] = True

# Add   185.60.216.0/22    ( 185.60.216.0 - 185.60.219.255)
for bignum in range(216, 219):
    for smallnum in range(0,255): 
      fb_ip_list['185.60.' + str(bignum) + '.' + str(smallnum)] = True
# Add    179.60.192.0/22     (  179.60.192.0 - 179.60.195.255)
for bignum in range(192, 195):
    for smallnum in range(0,255): 
      fb_ip_list['179.60.' + str(bignum) + '.' + str(smallnum)] = True

# Add     173.252.64.0/18     ( 173.252.64.0- 173.252.127.255)
for bignum in range(64, 127):
    for smallnum in range(0,255): 
      fb_ip_list['173.252.' + str(bignum) + '.' + str(smallnum)] = True

# Add    103.4.96.0/22     ( 103.4.96.0  - 103.4.99.255)
for bignum in range(96, 99):
    for smallnum in range(0,255): 
      fb_ip_list['103.4.' + str(bignum) + '.' + str(smallnum)] = True

# Add    45.64.40.0/22     ( 45.64.40.0  - 45.64.43.255)
for bignum in range(40, 43):
    for smallnum in range(0,255): 
      fb_ip_list['45.64.' + str(bignum) + '.' + str(smallnum)] = True


#returns application for ip address given pre-set IP address lists
def find_application_type(ip):
  if fb_ip_list.get(ip, None):
    return 'Facebook'
  if reddit_ip_list.get(ip, None):
    return 'Reddit'
  if quora_ip_list.get(ip, None):
    return 'Quora' 
  return 'OTHER'

def is_cs_traffic(cs_ips, ip):

	partial_ip = '.'.join(ip.split('.')[:-1]);

	if cs_ips.get(partial_ip, None):
		return True
	else:
		return False

def read_tcpdrump(timestamp, end_time):

	global discarded_packets
	if discarded_packets is None:
		discarded_packets = 0

	for line in sys.stdin:
		l = line.split()
		
		try:
			# Gets packet timestamp. Date is hardcoded because tcpdump only provides time.
			# This only works for measurements within the same day
			packet_ts = datetime.strptime(
				('2015-11-17T' + l[0][:8] + 'Z'), '%Y-%m-%dT%H:%M:%SZ'
			)

			packet_type = l[1].replace(',','');
			
			#IP packets
			if packet_type == 'IP':
				protocol = re.search( r'proto (\w+)', line).group(1)
				# Getting source IP and port (if available)
				# src = re.search( r'(\w+) >', line).group(1).split('.')
				src = re.search( r'([0-9.]*[0-9]+) >', line).group(1).split('.')
				
				src_ip = '.'.join(src[0:4])
				src_port = None
				if len(src) > 4: # has port
					src_port = src[4].replace(":", "")


				# Getting destination IP and port (if available)
				# dst = re.search(r'> (\w+)', line).group(1).split('.')
				dst = re.search( r'> ([0-9.]*[0-9]+)', line).group(1).split('.')
				dst_ip = '.'.join(dst[0:4])
				dst_port = None
				if len(dst) > 4: # has port
					dst_port = dst[4].replace(":", "")

				length = int(re.search( r'length (\w+)', line).group(1)) # TODO change this to get size inside parenthesis
			elif packet_type == 'ARP':
				discarded_packets = discarded_packets + 1
				

			else:
				raise ValueError()

			application_type = find_application_type(dst_ip)

			if application_type == 'OTHER':
				application_type = find_application_type(src_ip)

			packet_saved = False
			print str(packet_ts) + str(timestamp) + str(end_time)
			if (packet_ts >= timestamp) and (packet_ts <= end_time):
				
					if (src_ip in data) :
				#		 print 'src ' + src_ip + 'mac address ' + data[src_ip]
				#		 print 'timestamp ' + str(packet_ts)
				#		 print 'protocol ' + protocol
				#		 print 'mac address ' + data[src_ip]
				#		 print 'src ' + src_ip
				#		 print 'dst ' + dst_ip
				#		 print 'length ' + str(length)
				#		 print 'from CS: ' + str(is_cs_traffic(cs_ip_list, src_ip))
				#		 print 'to CS: ' + str(is_cs_traffic(cs_ip_list, dst_ip))
							pkt = Packet(
								mac_address=ap,
								timestamp=packet_ts,
								src_ip=src_ip,
								dst_ip=dst_ip,
								protocol=protocol,
								src_port=src_port,
								dst_port=dst_port,
								from_cs=is_cs_traffic(cs_ip_list, src_ip),
								to_cs=is_cs_traffic(cs_ip_list, dst_ip),
								size=length,
								application_type= application_type,
								client_ip = src_ip

								)
							pkt.save()
				
					elif (dst_ip in data) :
				#		 print 'dst ' + dst_ip + 'mac address ' + data[dst_ip]
				#		 print 'timestamp ' + str(packet_ts)
				#		 print 'protocol ' + protocol
				#		 print 'mac address ' + data[dst_ip]
				#		 print 'src ' + src_ip
				#		 print 'dst ' + dst_ip
				#		 print 'length ' + str(length)
				#		 print 'from CS: ' + str(is_cs_traffic(cs_ip_list, src_ip))
				#		 print 'to CS: ' + str(is_cs_traffic(cs_ip_list, dst_ip))
						pkt = Packet(
								mac_address=ap,
								timestamp=packet_ts,
								src_ip=src_ip,
								dst_ip=dst_ip,
								protocol=protocol,
								src_port=src_port,
								dst_port=dst_port,
								from_cs=is_cs_traffic(cs_ip_list, src_ip),
								to_cs=is_cs_traffic(cs_ip_list, dst_ip),
								size=length,
								application_type= application_type,
								client_ip = dst_ip

								)
							pkt.save()				
				
					else :
						discarded_packets = discarded_packets + 1

			elif packet_ts > end_time:
				break # breaks the loop if packet timestamp is out of the 10sec interval
					  # tcpdump_ref_line will save next line to be read for the next 10sec interval

		except BaseException as e:
			print line,
			# print "Unexpected error:", sys.exc_info()[0]
			discarded_packets = discarded_packets + 1
			continue

# Gets the starting/reference timestamp
with open('cn-wlc.txt') as g:
	for line in g:
		ref_timestamp = datetime.strptime(
			line.split()[0], '%Y-%m-%dT%H:%M:%SZ'
		)
		break

# Goes over a list of AP-IP from SNMPwalk
with open('cn-wlc.txt') as f:
	for line in f:
		l = line.split()
		timestamp = datetime.strptime(
			l[0], '%Y-%m-%dT%H:%M:%SZ'
		)
		try:
	 		if len(l) < 3:
	 			if not invalid:
	 				invalid = True
					print 'invalid is true'
	 			continue

			ip = l[1]
			ap = l[2]
#			data[ip] = ap
		except IndexError as e:
			continue


		# If timestamp read is different, go over tcpdump data for the 10sec interval that is ending
		if timestamp != ref_timestamp:
			print 'timestamp changed'
			# read_tcpdump returns False if it didn't read at least one line of a file
			# i.e., the starting line might have been out of range, which triggered the set up
			# of the next file to be read
			
			if invalid:
				print 'invalid is true'
				data = prevdata
				invalid = False
			else :
				print 'invalid is false'
				prevdata = data

			print('#######################')
		#	print(str(len(data)))
		#	for keys,values in data.items():
   		#		 print(keys + ' ' + values)

			read_tcpdrump(ref_timestamp, timestamp)
			ref_timestamp = timestamp
			data = {}

		if not invalid:
		#	print 'invalid is not true'
			if ip not in data:
				data[ip] = ap

print('discarded_packets' + str(discarded_packets))
