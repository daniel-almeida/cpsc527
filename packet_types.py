# This program will go over the first 10k lines of a tcpdump 
# and extract one example of each type of packet found.

# It prints the list of types and creates the packet_types.txt file 
# containing one packet example (sample) for each type.

import sys
import re

types_of_packets = []

f = open('packet_types.txt','w')

i = 0
for line in sys.stdin:
	if i == 10000:
		break

	searchObj = re.search( r'proto (\w+)', line)

	if searchObj:
		protocol = searchObj.group(1)
		if protocol not in types_of_packets:
			types_of_packets.append(protocol)
			f.write(line)

	i += 1
f.close()
print 'List of protocols found: '
print types_of_packets

